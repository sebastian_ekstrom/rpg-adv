import attack as A

class Weapon():
    def __init__(self, name, attack):
        self.name = name
        self.attack = attack

    def use(self, ui, user, target):
        self.attack.use(ui, user, target)


hammer = Weapon('Hammer', A.Phys_Atk('', 4, 0))
ultimate_death_ray = Weapon('Ultimate Death Ray', A.Mag_Atk('', 9999, 0))
rubber_duck = Weapon('Rubber Duck', A.Phys_Atk('', 1, 0))
