from abc import ABCMeta, abstractmethod

class BaseItem():
    __metaclass__ = ABCMeta

    def __init__(self, name, description, use_msg):
        self.name = name
        self.desc = description
        self.use_msg = use_msg

    def print_description(self):
        print(self.desc)

    @abstractmethod
    def use(self, user):
        pass

class HealingItem(BaseItem):
    def __init__(self, name, description, use_msg, hp_restored, mp_restored):
        super().__init__(name, description, use_msg)
        self.hp_res = hp_restored
        self.mp_res = mp_restored

    def use(self, user):
        print(use_msg)
        if self.hp_res != 0:
            hp_res = min(self.hp_res, user.max_hp - user.hp)
            user.hp += hp_res
            print('%s restored %d HP!' % (user.name, hp_res))
        if self.mp_res != 0:
            mp_res = min(self.mp_res, user.max_mp - user.mp)
            user.mp += mp_res
            print('%s restored %d MP!' % (user.name, mp_res))

class ItemEntry():
    def __init__(self, name, item, amount=1):
        self.name = name
        self.item = item
        self.amount = amount

    def use(self, user):
        self.item.use(user)
        self.amount -= 1
        return self.amount == 0

    def add(self, amount=1):
        self.amount += amount
