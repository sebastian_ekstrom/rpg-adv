import curses

BLUE = 1
CYAN = 2
GREEN = 3
MAGENTA = 4
RED = 5
WHITE = 6
YELLOW = 7

colors = {
    BLUE:       curses.COLOR_BLUE,
    CYAN:       curses.COLOR_CYAN,
    GREEN:      curses.COLOR_GREEN,
    MAGENTA:    curses.COLOR_MAGENTA,
    RED:        curses.COLOR_RED,
    WHITE:      curses.COLOR_WHITE,
    YELLOW:     curses.COLOR_YELLOW}
    
FLOORWIN_ROWS = 25
FLOORWIN_COLS = 35

MENUWIN_ROWS = 10
MENUWIN_COLS = 35

MSGWIN_ROWS = 37
MSGWIN_COLS = 35

PLAYER_MARKER = '@'

CHAR_UP = 'w'
CHAR_DOWN = 's'
CHAR_CONFIRM = ' '

class GameUI:
    
    def __init__(self, stdscr):
        stdscr.clear()
        
        curses.curs_set(0)
        
        self.floor_window = curses.newwin(FLOORWIN_ROWS+2, FLOORWIN_COLS+2, 0, 0)
        self.floor_window.border()
        self.floor_window.refresh()
        
        self.msg_window = curses.newwin(MSGWIN_ROWS+2, MSGWIN_COLS+2, 0, FLOORWIN_COLS+2)
        self.msg_window.border()
        self.msg_window.refresh()
        self.msg_line = 0
        
        self.menu_window = curses.newwin(MENUWIN_ROWS+2, MENUWIN_COLS+2, FLOORWIN_ROWS+2, 0)
        self.menu_window.border()
        self.menu_window.refresh()
        
    def draw_floor(self, floor, player_row, player_col):
        self.floor_window.addstr(0, FLOORWIN_COLS+1-len(floor.get_name()), floor.get_name())
        
        row_offset = player_row - FLOORWIN_ROWS//2
        col_offset = player_col - FLOORWIN_COLS//2
        for row in range(FLOORWIN_ROWS):
            for col in range(FLOORWIN_COLS):
                if row == FLOORWIN_ROWS//2 and col == FLOORWIN_COLS//2:
                    marker = PLAYER_MARKER
                else:
                    marker = floor.get_marker(row + row_offset, col + col_offset)
                self.floor_window.addch(row+1, col+1, marker)
        self.floor_window.refresh()
        
    def wait_for_input(self):
        return self.floor_window.getch()
        
    def print_message(self, msg):
        for line in msg.splitlines():
            self.print_message_line(line)
        
    def print_message_line(self, msg):
        window = self.msg_window
        cols = MSGWIN_COLS+2
        lines = MSGWIN_ROWS+2
            
        while msg:
            if self.msg_line == (lines - 2):
                window.move(1,1)
                window.deleteln()
                window.move(self.msg_line, 1)
                window.addstr(" " * (cols - 2))
                window.border()
            else:
                self.msg_line += 1

            if len(msg) > cols - 2:
                part = msg[:cols - 1]
                split = part.rfind(' ')
                #if split == -1:
                #    split = cols - 2
                line = part[:split]
                msg = msg[len(line):]
                msg = msg.lstrip()
            else:
                line = msg
                msg = None
            
            window.move(self.msg_line, 1)
            window.addstr(line, WHITE)
            window.refresh()
            
    def select_from_menu(self, options):
        self.menu_window.clear()
        self.menu_window.border()
        sel_char = '>'
        n_options = len(options)
        for i in range(n_options):
            self.menu_window.addstr(i+1, 3, options[i])
            
        row = 0
        self.menu_window.addch(row+1, 1, sel_char)
        self.menu_window.refresh()
        
        while True:
            ch = chr(self.wait_for_input())
            if ch == CHAR_UP or ch == CHAR_DOWN:
                self.menu_window.addch(row+1, 1, ' ')
                if ch == CHAR_UP:
                    row -= 1
                else:
                    row += 1
                row %= n_options
                self.menu_window.addch(row+1, 1, sel_char)
                self.menu_window.refresh()
            elif ch == CHAR_CONFIRM:
                self.menu_window.clear()
                self.menu_window.border()
                self.menu_window.refresh()
                return row

