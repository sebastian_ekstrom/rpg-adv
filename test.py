import curses

from floor import Floor
from world import World
from ui import GameUI

import enemy as E
import character as C

layout1 = """#######
#   # #
# # # #
#     #
### # #
#     #
#######"""

layout2 = """#########
#       #
#       #
#       #
#       #
#       #
#       #
#       #
#########"""

def main(stdscreen):
    f1 = Floor(layout1, 'Floor 1')
    f1.add_enemies(3, 5, [E.imp, E.imp])
    f1.add_exit(1, 5, 1, 4, 'floor2')
    f1.add_item(1, 1, 'b')
    f1.add_message(3, 1, 'Hello world\ntest\ntest2', True)
    
    f2 = Floor(layout2, 'Floor 2')
    f2.add_exit(1, 4, 1, 5, 'floor1')
    
    ui = GameUI(stdscreen)
    '''
    ui.print_message('Choose an option')
    options = ['Option a', 'Option b', 'Option c']
    o = ui.select_from_menu(options)
    ui.print_message(options[o] + ' was chosen')
    '''
    c = C.Character('test')
    w = World(ui, {'floor1': f1, 'floor2': f2}, c, 5, 1, 'floor1')
    while(True):
        w.wait_for_input()
    
curses.wrapper(main)
