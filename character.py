import random

import weapon as wpn
import attack as atk
import item

class Character():
    def __init__(self, name, max_hp=10, max_mp=5, atk=1, df=0, mag=1, res=0, weapon=None, skills=[atk.attack]):
        self.name = name
        self.max_hp = max_hp
        self.hp = max_hp
        self.max_mp = max_mp
        self.mp = max_mp
        self.atk = atk
        self.df = df
        self.mag = mag
        self.res = res
        self.weapon = weapon
        self.skills = skills
        self.inventory = {}

    def to_string(self):
        return self.name + '\nHP: ' + str(self.hp) + '/' + str(self.max_hp) + '\nMP: ' + str(self.mp) + '/' + str(self.max_mp)

    def moves_to_string(self):
        result = '0: Focus\n'
        if self.weapon:
            result += '1: ' + self.weapon.name + '\n'
            for nbr, skill in enumerate(self.skills):
                result += str(nbr + 2) + ': ' + skill.to_string() + '\n'
        else:
            for nbr, skill in enumerate(self.skills):
                result += str(nbr + 1) + ': ' + skill.to_string() + '\n'
        return result.strip()
        
    def moves_to_list(self):
        result = ['Focus']
        if self.weapon:
            result.append(self.weapon.name)
        for skill in self.skills:
            result.append(skill.to_string())
        return result

    def valid_moves(self):
        return [i for i in range(len(self.skills) + 1 + bool(self.weapon))]

    def use_move(self, ui, index, target):
        if index == 0:
            self.focus(ui)
        elif self.weapon:
            if index == 1:
                self.attack(ui, target)
            else:
                self.use_skill(ui, index-2, target)
        else:
            self.use_skill(ui, index-1, target)


    def attack(self, ui, target):
        if self.weapon:
            ui.print_message(self.name + ' attacks ' + target.name + ' with ' + self.weapon.name + '!')
            self.weapon.use(ui, self, target)
            return True
        return False

    def focus(self, ui):
        restore = min(self.max_mp // 4, self.max_mp - self.mp)
        self.mp += restore
        ui.print_message(self.name + ' focuses to restore ' + str(restore) + ' MP!')

    def can_use_skill(self, index):
        return index < len(self.skills) and self.mp >= self.skills[index].cost

    def get_skill(index):
        if len(self.skills) > index:
            return self.skills[index]
        raise Exception("Invalid skill index")

    def use_skill(self, ui, index, target):
        if len(self.skills) > index:
            skill = self.skills[index]
            if self.can_use_skill(index):
                ui.print_message(self.name + ' uses ' + skill.name + ' on ' + target.name + '!')
                skill.use(ui, self, target)
                return True
            return False
        raise Exception("Invalid skill index")

    def hurt(self, ui, dmg):
        if dmg <= 0:
            ui.print_message(self.name + ' takes no damage!', True)
        elif dmg >= self.hp:
            self.hp = 0
            ui.print_message(self.name + ' takes ' + str(dmg) + ' damage!\n' + self.name + ' is defeated!')
        else:
            self.hp -= dmg
            ui.print_message(self.name + ' takes ' + str(dmg) + ' damage!')


    def possible_moves(self):
        result = []
        if self.weapon:
            result.append(0)
        for i in range(len(self.skills)):
            if self.can_use_skill(i):
                result.append(i + 1)
        return result

    def random_attack(self, target):
        possible = self.possible_moves()
        if len(possible) > 0:
            action = possible[random.randint(0, len(possible) - 1)]
            if action == 0:
                if self.weapon:
                    self.attack(target)
            else:
                if self.can_use_skill(action - 1):
                    self.use_skill(action - 1, target)
        else:
            self.focus()

    def add_to_inv(self, item):
        if item.name in inventory:
            inventory[item.name].add(item)
        else:
            inventory[item.name] = item.ItemEntry(item)

    def use_item(self, item_name):
        if item_name in inventory:
            left = inventory[item_name].use(self)
            if not left:
                inventory.pop(item_name)

def create_character():
    name = input('Enter name: ')
    return Character(name, skills = [atk.attack])


# test characters

john = Character('John', 120, 10, 18, 16, 23, 10, wpn.hammer, [atk.windy_thing])
jade = Character('Jade', 100, 12, 3, 5, 30, 23, skills = [atk.duck_storm, atk.piano_rain])
