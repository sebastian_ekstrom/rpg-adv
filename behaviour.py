import random

class RandomBehaviour():
    # randomly picks from all possible moves each turn
    def next_move(self, user):
        return random.randint(0, len(user.skills) + bool(user.weapon) - 1)

class RandomBehaviour2():
    # randomly picks from a list of moves each turn
    def __init__(self, skills_to_use):
        self.skills = skills_to_use

    def next_move(self, user):
        index = random.randint(0, len(self.skills)-1)
        return self.skills[index]

class CycleBehaviour():
    # goes through all possible moves sequentially
    def __init__(self):
        self.turn_counter = 0

    def next_move(self, user):
        index = self.turn_counter
        self.turn_counter += 1
        self.turn_counter %= (len(user.skills) + bool(user.weapon))
        return index

class CycleBehaviour2():
    # goes through a list of moves sequentially
    def __init__(self, skills_to_use):
        self.skills = skills_to_use
        self.turn_counter = 0

    def next_move(self, user):
        index = self.turn_counter
        self.turn_counter += 1
        self.turn_counter %= len(self.skills)
        return index

class TurnsBehaviour():
    # does one behaviour for a set number of turns, and then switches to another
    def __init__(self, behaviour1, behaviour2, turns):
        self.b1 = behaviour1
        self.b2 = behaviour2
        self.turns = turns
        self.turn_counter = 0

    def next_move(self, user):
        if self.turn_counter < self.turns:
            index = self.b1.next_move(user)
            self.turn_counter += 1
            self.turn_counter %= self.turns
        else:
            index = self.b2.next_move(user)
        return index

class TurnsBehaviourCycle():
    # cycles through a list of behaviours using a list of turn counts to determine how long to use each one
    def __init__(self, behaviours, turn_counts):
        if len(behaviours) != len(turn_counts):
            raise Exception('Lengths of behaviour list and turn counts list do not match')
        self.behaviours = behaviours
        self.turn_counts = turn_counts
        self.turn_counter = 0
        self.current = 0

    def next_move(self, user):
        index = self.behaviours[current].next_move(user)
        self.turn_counter += 1
        if self.turn_counter == self.turn_counts[current]:
            self.turn_counter = 0
            self.current += 1
            self.current %= len(self.turn_counts)
        return index

class LifeBehaviour():
    # does one behaviour until less than a certain percentage of life left, and then switches to another
    def __init__(self, behaviour1, behaviour2, life_percent):
        self.b1 = behaviour1
        self.b2 = behaviour2
        self.lp = life_percent

    def next_move(self, user):
        if user.max_hp * self.lp <= user.hp:
            return self.b1.next_move(user)
        else:
            return self.b2.next_move(user)
