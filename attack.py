class Attack():
    def __init__(self, name, power, cost, fixed = False, pierce = False):
        self.name = name
        self.power = power
        self.cost = cost
        self.fixed = fixed
        self.pierce = pierce

    def to_string(self):
        result = self.name
        if self.cost != 0:
            result += ' (' + str(self.cost) + ' MP)'
        return result

    def attack(self, ui, user, target, damage, free = False):
        if free or user.mp >= self.cost:
            target.hurt(ui, int(damage))
            user.mp -= self.cost
            return True
        return False

class Phys_Atk(Attack):
    def use(self, ui, user, target, free = False):
        dmg = self.power
        if not self.fixed:
            dmg += user.atk
        if not self.pierce:
            dmg -= target.df
        return self.attack(ui, user, target, dmg, free)


class Mag_Atk(Attack):
    def use(self, ui, user, target, free = False):
        dmg = self.power
        if not self.fixed:
            dmg += user.mag
        if not self.pierce:
            dmg -= target.res
        return self.attack(ui, user, target, dmg, free)

class Comp_Atk(Attack):
    def use(self, ui, user, target, free = False):
        dmg = self.power
        if not self.fixed:
            dmg += (user.atk + user.mag)
        if not self.pierce:
            dmg -= (target.df + target.res)
        return self.attack(ui, user, target, dmg, free)

class Cust_Atk(Attack):
    def __init__(self, name, power, cost, atk_mul, mag_mul, def_mul, res_mul):
        super(name, power, cost)
        self.atk_mul = atk_mul
        self.mag_mul = mag_mul
        self.def_mul = def_mul
        self.res_mul = res_mul

    def use(self, ui, user, target, free = False):
         dmg = self.power + self.atk_mul * user.atk + self.mag_mul * user.mag - self.def_mul * target.df - self.res_mul * target.res
         return self.attack(ui, user, target, dmg, free)

attack = Phys_Atk("Attack", 0, 0)

duck_storm = Mag_Atk("Duck Storm", 5, 3)
piano_rain = Phys_Atk("Piano Rain", 52, 5, fixed = True)
flatten = Comp_Atk("Flatten", 10, 4)
windy_thing = Mag_Atk("The Windy Thing", 30, 4)
master_spark = Mag_Atk("Master Spark", 1000, 30)
