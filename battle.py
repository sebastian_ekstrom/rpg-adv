import character as C
import enemy as E

import copy
import os

def battle(player, enemies, ui):
    enemies = [copy.deepcopy(e) for e in enemies]
    os.system('clear')
    while True:
        print_battle_status(ui, player, enemies)
        player_move(ui, player, enemies)
        if all_defeated(enemies):
            ui.print_message('You won the battle!')
            return True
        for e in enemies:
            if e.hp > 0:
                enemy_move(ui, e, player)
        if player.hp == 0:
            ui.print_message('You lost the battle...')
            return False


def print_battle_status(ui, player, enemies):
    ui.print_message(player.to_string())
    for e in enemies:
        if e.hp > 0:
            ui.print_message(e.to_string())

def all_defeated(enemies):
    for e in enemies:
        if e.hp > 0:
            return False
    return True


def player_move(ui, player, enemies):
    action, needs_target = get_player_action(ui, player)
    if needs_target:
        target = get_player_target(ui, player, enemies)
        player.use_move(ui, action, target)
        if target.hp <= 0:
            enemies.remove(target)
    else:
        player.use_move(ui, action, None)


def get_player_action(ui, player):
    while(True):
        move = ui.select_from_menu(player.moves_to_list())
        if move == 0:
            return 0, False
        if player.weapon:
            if move == 1:
                return 1, True
            else:
                if player.can_use_skill(move-2):
                    return move, True
                else:
                    ui.print_message('Not enough MP!')
                    continue
        else:
            if player.can_use_skill(move-1):
                return move, True
            else:
                ui.print_message('Not enough MP!')
                continue

def get_player_target(ui, player, enemies):
    #if len(enemies) == 1:
    #    return enemies[0]
    target = ui.select_from_menu([e.name for e in enemies])
    return enemies[target]


def enemy_move(ui, enemy, player):
    move = enemy.get_next_move()
    enemy.use_move(ui, move, player)
