import ui
import floor
import battle

CHAR_UP = 'w'
CHAR_LEFT = 'a'
CHAR_DOWN = 's'
CHAR_RIGHT = 'd'

class World():
    def __init__(self, ui, floors, player, start_row, start_col, start_floor):
        self.ui = ui
        self.floors = floors
        self.player = player
        self.c_row = start_row
        self.c_col = start_col
        self.c_floor = floors[start_floor]
        self.c_floor.reveal(start_row, start_col)
        self.draw_floor()
        
    def draw_floor(self):
        self.ui.draw_floor(self.c_floor, self.c_row, self.c_col)
        
    def wait_for_input(self):
        command = chr(self.ui.wait_for_input())
        if command == CHAR_UP:
            self.move(self.c_row-1, self.c_col)
        elif command == CHAR_LEFT:
            self.move(self.c_row, self.c_col-1)
        elif command == CHAR_DOWN:
            self.move(self.c_row+1, self.c_col)
        elif command == CHAR_RIGHT:
            self.move(self.c_row, self.c_col+1)
        elif command == 'q':
            exit()
    
    
    def move(self, row, col):
        if self.c_floor.is_walkable(row, col):
            self.c_row = row
            self.c_col = col
            self.c_floor.reveal(row, col)
            self.ui.draw_floor(self.c_floor, self.c_row, self.c_col)
            self.handle_tile()
        
    def change_floor(self, row, col, floor):
        self.c_floor = self.floors[floor]
        self.c_row = row
        self.c_col = col
        self.c_floor.reveal(row, col)
        self.ui.draw_floor(self.c_floor, self.c_row, self.c_col)
        
    def handle_tile(self):
        tile_type = self.c_floor.get_tile_type(self.c_row, self.c_col)
        if tile_type == floor.TILE_ENEMY:
            self.handle_enemy_tile()
        elif tile_type == floor.TILE_ITEM:
            self.handle_item_tile()
        elif tile_type == floor.TILE_EXIT:
            self.handle_exit_tile()
        elif tile_type == floor.TILE_MESSAGE:
            self.handle_message_tile()
        elif tile_type == floor.TILE_EVENT:
            self.handle_event_tile()
            
    def handle_enemy_tile(self):
        enemies = self.c_floor.get_enemies(self.c_row, self.c_col)
        win = battle.battle(self.player, enemies, self.ui)
        if win:
            self.c_floor.set_cleared(self.c_row, self.c_col)
        else:
            self.battle_lost()
        
    def handle_item_tile(self):
        pass
        
    def handle_exit_tile(self):
        action = self.ui.select_from_menu(['Enter', "Don't enter"])
        if action == 0:
            row, col, floor = self.c_floor.get_exit(self.c_row, self.c_col)
            self.change_floor(row, col, floor)
        
    def handle_message_tile(self):
        msg, persistent = self.c_floor.get_message(self.c_row, self.c_col)
        self.ui.print_message(msg)
        if not persistent:
            self.c_floor.set_cleared(self.c_row, self.c_col)
        
    def handle_event_tile(self):
        event_fcn, persistent = self.c_floor.get_event(self.c_row, self.c_col)
        event_fcn(self)
        if not persistent:
            self.c_floor.set_cleared(self.c_row, self.c_col)
            
    def battle_lost(self):
        pass
