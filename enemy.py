import character as C
import behaviour as B
import attack as A

class Enemy(C.Character):

    def __init__(self, name, max_hp, atk, df, mag, res, weapon = None, skills = [], behaviour = B.RandomBehaviour()):
        super().__init__(name, max_hp, 0, atk, df, mag, res, weapon, skills)
        self.behaviour = behaviour

    def to_string(self):
        return self.name + '\nHP: ' + str(self.hp) + '/' + str(self.max_hp)

    def get_next_move(self):
        return self.behaviour.next_move(self)

    def use_move(self, ui, index, target):
        if self.weapon:
            if index == 1:
                self.attack(ui, target)
            else:
                self.use_skill(ui, index-1, target)
        else:
            self.use_skill(ui, index, target)

    def use_skill(self, ui, index, target):
        if len(self.skills) > index:
            skill = self.skills[index]
            ui.print_message(self.name + ' uses ' + skill.name + ' on ' + target.name + '!')
            skill.use(ui, self, target, True)
            return True
        raise Exception("Invalid skill index")

imp = Enemy('Imp', 2, 1, 0, 0, 0, skills = [A.attack])
jade = Enemy('Jade', 40, 3, 5, 30, 23, skills = [A.duck_storm, A.piano_rain, A.flatten])
