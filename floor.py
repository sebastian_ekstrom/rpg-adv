TILE_UNKNOWN = -1
TILE_EMPTY = 0
TILE_WALL = 1
TILE_ENEMY = 2
TILE_ITEM = 3
TILE_EXIT = 4
TILE_MESSAGE = 5
TILE_EVENT = 6

MARKERS = '.#&$O!! '

class Floor():
    
    def __init__(self, layout, name):
        self.name = name
        
        lines = layout.splitlines()
        self.nrows = len(lines)
        self.ncols = 0
        for line in lines:
            self.ncols = max(self.ncols, len(line))
            
        self.layout = []
        for row in range(self.nrows):
            self.layout.append([])
            for col in range(self.ncols):
                if len(lines[row]) <= col:
                    tile_type = TILE_EMPTY
                elif lines[row][col] == ' ':
                    tile_type = TILE_EMPTY
                else:
                    tile_type = TILE_WALL
                self.layout[row].append(tile_type)
                
        self.revealed = [[False for col in range(self.ncols)] for row in range(self.nrows)]
        
        self.enemies = {}
        self.items = {}
        self.cleared = {}
        self.exits = {}
        self.events = {}
        self.messages = {}
        
    def get_name(self):
        return self.name
        
    def add_enemies(self, row, col, enemies):
        self.assert_index(row, col)
        if not self.is_empty(row, col):
            raise LayoutException('Enemies can only be placed on an empty tile')
        self.enemies[(row, col)] = enemies
        self.cleared[(row, col)] = False
        self.layout[row][col] = TILE_ENEMY
        
    def get_enemies(self, row, col):
        if (row, col) in self.enemies:
            return self.enemies[(row, col)]
        return None
        
    def add_item(self, row, col, item):
        self.assert_index(row, col)
        if not self.is_empty(row, col):
            raise LayoutException('Items can only be placed on an empty tile')
        self.items[(row, col)] = item
        self.cleared[(row, col)] = False
        self.layout[row][col] = TILE_ITEM
    
    def get_item(self, row, col):
        if (row, col) in self.items:
            return self.items[(row, col)]
        return None
        
    def add_exit(self, row, col, dest_row, dest_col, dest_floor):
        self.assert_index(row, col)
        if not self.is_empty(row, col):
            raise LayoutException('Exits can only be placed on an empty tile')
        self.exits[(row, col)] = (dest_row, dest_col, dest_floor)
        self.layout[row][col] = TILE_EXIT
        
    def get_exit(self, row, col):
        if (row, col) in self.exits:
            return self.exits[(row, col)]
        return None
        
    def add_message(self, row, col, message, persistent = True):
        self.assert_index(row, col)
        if not self.is_empty(row, col):
            raise LayoutException('Messages can only be placed on an empty tile')
        self.messages[(row, col)] = (message, persistent)
        self.layout[row][col] = TILE_MESSAGE
        
    def get_message(self, row, col):
        if (row, col) in self.messages:
            return self.messages[(row, col)]
        return None
        
    def add_event(self, row, col, event_function, persistent = False):
        self.assert_index(row, col)
        if not self.is_empty(row, col):
            raise LayoutException('Events can only be placed on an empty tile')
        self.event[(row, col)] = (event_function, persistent)
        self.layout[row][col] = TILE_EVENT
        
    def get_event(self, row, col):
        if (row, col) in self.events:
            return self.events[(row, col)]
        return None
        
    def set_cleared(self, row, col):
        self.cleared[(row, col)] = True
        self.layout[row][col] = TILE_EMPTY
        
    def is_clear(self, row, col):
        if not (row, col) in self.cleared:
            return False
        return self.cleared[(row, col)]
        
    def is_walkable(self, row, col):
        return self.is_index_valid(row, col) and self.layout[row][col] != TILE_WALL
        
    def is_empty(self, row, col):
        return self.layout[row][col] == TILE_EMPTY
        
    def get_tile_type(self, row, col):
        if not self.is_index_valid(row, col) or self.is_clear(row, col):
            return TILE_EMPTY
        return self.layout[row][col]
        
    def get_marker(self, row, col):
        if not self.is_index_valid(row, col):
            return MARKERS[TILE_UNKNOWN]
        if not self.revealed[row][col]:
            return MARKERS[TILE_UNKNOWN]
        return MARKERS[self.layout[row][col]]
        
    def is_index_valid(self, row, col):
        return row >= 0 and row < self.nrows and col >= 0 and col < self.ncols
        
    def assert_index(self, row, col):
        if not self.is_index_valid(row, col):
            raise LayoutException('Invalid index')
            
    def reveal(self, row, col, range_=1):
        for r_offset in range(-range_, range_+1):
            for c_offset in range(-range_, range_+1):
                if self.is_index_valid(row + r_offset, col + c_offset):
                    self.revealed[row+r_offset][col+c_offset] = True
        
    def __str__(self):
        return '\n'.join([''.join([self.get_marker(row, col) for col in range(self.ncols)]) for row in range(self.nrows)])
        
class LayoutException(Exception):
    pass
        
        
